# README #
Mackson Consulting ADS RFQ Prototype  
Prototype URL  http://fdabpa.cloudapp.net:8080/fda-bpa/mackson/Welcome.action
ADS RFQ under Pool 2, Development

### Instructions to build and test Mackson FDA BPA project ###

1. Install Java 7 (jdk 1.7)
2. Install apache-maven-3.3.3
3. To build and run unit tests:
	* from command prompt, navigate to fdabpaopensource/code
        * run "mvn clean package". 
4. To integration test:
	* from command prompt, navigate to fdabpaopensource/code
        * run "mvn jetty:run-war" 
	* point browser at http://localhost:8080/mackson
	
### Technical Approach ###
Where applicable below, we have highlighted evidence of meeting U.S. Digital Playbook requirements explicitly, ex [DPB-1].

	
### Team ###
After reviewing the RFQ, we assembled a cross functional team tailored to the requirements. The team had experience with FDA, open technologies and the design principles espoused in the RFQ and the Digital Playbook. [DPB-7] The team included the following labor categories:

* Technical Architect
* Frontend Web Developer
* Backend Web Developer
* DevOps Engineer 

We assigned a Technical Architect with overall responsibility for the prototype. [DPB-6] We designated a proxy user, who had extensive knowledge of FDA and enabled our team to define realistic user stories. [DPB-1]

### Design ###
The Technical Architect and Front End Developer each had deep FDA domain knowledge. We performed additional research once the RFQ and data set were released to confirm our understanding of what would be good real world use scenarios. After familiarizing ourselves with the data, we developed three scenarios to support a hypothetical FDA drug specialist.  Those scenarios accounted for the whole user experience from start to finish. [DPB-2] Based on the scenarios, human-centered design principles were used to create user stories including screen mockups (Fig 1, 2). Our Technical Architect and Interaction Designer collaborated on the viability of the stories given the data and made minor adjustments. We created mockups that were attached to the appropriate tickets in Jira and placed in the repository. The proxy user was involved throughout design and development including cooperative design and usability testing. Due to time constraints, we implemented portions of the scenarios, and our design accounted for a complete functional experience. 

##### Drug Search Mockup  - Fig 1 #####
![Alt text](https://bitbucket.org/macksonteam/fdabpaopensource/raw/2f6537fa574d82e712e6036aa70d4685c50ea0bf/documents/FDADrugFinder_Design1.png?token=714e58c0abd7e98ccf369639a2ea2fc0a516ee1c "Drug Search Design")
##### Drug Details Mockup  - Fig 2 #####
![Alt text](https://bitbucket.org/macksonteam/fdabpaopensource/raw/2f6537fa574d82e712e6036aa70d4685c50ea0bf/documents/FDADrugFinder_Design2.png)

### Develop ###
We built our prototype using an agile and iterative process. [DPB-4]

Our DevOps Engineer created a project in our Jira repository where we documented user stories. We created additional tasks in Jira (Fig 3) to capture the DevOps task such as such as creating a new Jenkins build configuration and instantiating a new app server in Azure. 
We held a Sprint Planning session and setup a new Sprint with a timebox to end on the morning of June 25th. We assigned user stories and scheduled a daily standup meeting.  During each daily call, each team member reported on their progress an updated our Scrum Board in Jira. We captured any new issues in the backlog to be triaged by the Technical Architect. Our Proxy User also provided feedback. The Technical Arcthitect decided whether proposed changes would be incorporated into the current sprint. We also updated the screen mockups as well and the backlog.

##### Agile Methodology & Scrum  - Fig 3 #####
![Alt text](https://bitbucket.org/macksonteam/fdabpaopensource/raw/619ec4b8295068fb9b634cd6a96dcf624e274119/readme/Jira1.png "Jira Project")

### Frameworks and Infrastructure ###
#### Open Technology Stack [DPB-8], [DPB-13] ####
Our team considered several options for an open source and license free technology stack. We decided on the following Linux / Java / Web implementation: 

* Ubuntu 14.04 LTS ([User License Agreement](http://www.ubuntu.com/about/about-ubuntu/licensing))
* Java OpenJDK 7 ([User License Agreement](http://www.oracle.com/technetwork/java/javase/documentation/otn-bcl-02april2013-1966219.pdf))
* Tomcat 7 ([User License Agreement](http://www.apache.org/licenses/LICENSE-2.0))
* Struts2 2.3 ([User License Agreement](https://struts.apache.org/maven/license.html))
* Log4j2 2.2 ([User License Agreement](https://logging.apache.org/log4j/2.0/license.html))
* JUnit 4.5 ([User License Agreement](http://junit.org/license.html))
* Bootstrap ([User License Agreement](https://github.com/twbs/bootstrap/blob/master/LICENSE))
* Leaflet – mapping js library ([User License Agreement](https://github.com/Leaflet/Leaflet/blob/master/LICENSE))
* Jquery ([User License Agreement](https://jquery.org/license/))
* Open Street Maps ([User License Agreement](http://www.openstreetmap.org/copyright))
* Nominatim geocoding service ([User License Agreement](http://www.openstreetmap.org/copyright))

##### Continuous Deployment - Fig 4 #####
![Alt text](https://bitbucket.org/macksonteam/fdabpaopensource/raw/619ec4b8295068fb9b634cd6a96dcf624e274119/readme/Jenkins1.png "Jenkins Project")

##### Continuous Testing  - Fig 5#####
![Alt text](https://bitbucket.org/macksonteam/fdabpaopensource/raw/619ec4b8295068fb9b634cd6a96dcf624e274119/readme/Jenkins2.png "Jenkins Unit Test")

#### Open Infrastructure [DPB-9] ####
Mackson uses continuous delivery pipelines employing a variety of tools hosted in different environments (private, AWS and Azure). For this effort, we stood up:

* __Ubuntu / Tomcat app server__ - Azure virtual environment
* __BitBucket Git repository__ - Azure virtual environment
* __Jenkins build server__ - Azure virtual environment
* __Jira backlog and issue tracker__ -  hosted by Atlassian

Our BitBucket Git repository was configured to accept commits that link back to Jira user stories. This enabled us to navigate to corresponding Jira user stories directly from commits while browsing the code. Our DevOps engineer created a Jenkins build job to poll Git for new commits, run all unit tests, publish the results to the Jenkins page and deploy the web app to the server (Fig 4, 5).  [DPB-10] With more time, we would implement more exhaustive automated continuous deployment mechanisms to configure the environments (Chef) and run tests (Selenium). We established Continuous Monitoring using Azure’s built in services. (Fig 6, 7) For the prototype, we configured uptime and response time metrics. We're monitoring these metrics from two different locations and alert emails are configured for any failures.

##### Continuous Monitoring  - Fig 6 #####
![Alt text](https://bitbucket.org/macksonteam/fdabpaopensource/raw/9d9702ad8148d5078323369146984c7729b3351a/readme/Azure1.png "Azure Virutual Server")

##### Continuous Monitoring  - Fig 7 #####
![Alt text](https://bitbucket.org/macksonteam/fdabpaopensource/raw/9d9702ad8148d5078323369146984c7729b3351a/readme/Azure2.png "Azure Virutual Server")

#### Open Tools ####

* Eclipse
* Maven 
* Junit


### Who do I talk to? ###
* mark.garrison@macksonconsulting.com