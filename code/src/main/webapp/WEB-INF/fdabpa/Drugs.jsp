<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title><s:text name="drugs"/></title>

  <%-- Bootstrap components --%>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="../css/mackson-theme.css">  

  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  <script src="../js/bootstrap.min.js"></script> 

  <style>
    .label {
      color: #000;
    }
  </style>

</head>
<body>
  <jsp:include page="Menu.jsp"/>
  
  <div class="container-fluid">  
    <div class="row row-notpadded">
      <div class="col-md-4">
        <div class="panel panel-default search-panel">
          <div class="panel-heading">Search</div>
          <div class="panel-body">
            <s:form action="caDrugsquery" method="post">  
                  <p><s:select key="drugType" list="drugTypes"
                                    class="dropdown form-control"
                                    headerKey="empty" headerValue="--- %{getText('drugType')} ---"
                                    required="false" /></p>                                          
                  <p><s:select key="drugClass" list="drugClasses"
                                    class="dropdown form-control"
                                    headerKey="empty" headerValue="--- %{getText('drugClass')} ---"
                                    required="false" /></p>                                          
                  <p><s:textfield key="manufacturer" class="form-control" placeholder="%{getText('manufacturer')}"/></p>                                          
                  <p><s:textfield key="product" class="form-control" placeholder="%{getText('product')}"/></p>                            
                  <p><s:submit class="btn btn-primary"/></p>              
            </s:form>
          </div>          
        </div>
      </div>      
      <div class="col-md-8">
        <div class="panel panel-default results-panel">
          <div class="panel-heading">Results</div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table">              
                <tr><td>
                   <display:table name="drugs">
                     <display:column property="splId" title= "SPL ID" href="caDrugDetails.action" paramId="splId" paramProperty="splId" />
                     <display:column property="productType" title="Product Type" />
                     <display:column property="productClass" title="Product Class" />
                     <display:column property="brandName" title="Product Name" />
                     <%--<display:column property="warnings" />--%>
                     <display:column property="manufacturerName" title="Mfr Name"/>
                   </display:table>
                </td></tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </div>
  <script>
      $(document).ready(function () {
        $('#navDrugs').addClass('active');
    });
  </script>
</body>
</html>