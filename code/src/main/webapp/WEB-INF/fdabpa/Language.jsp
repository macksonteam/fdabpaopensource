<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
<head>
    <title>Language</title>
    <%-- Bootstrap components --%>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/mackson-theme.css">

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="../js/bootstrap.min.js"></script> 
</head>

<body>
<jsp:include page="Menu.jsp"/>  

<h2 class="hidden"><s:property value="message"/></h2>

<h3>Languages</h3>
<ul>
    <li>
        <s:url id="url" action="caLanguage">
            <s:param name="request_locale">en</s:param>
        </s:url>
        <s:a href="%{url}">English</s:a>
    </li>
    <li>
        <s:url id="url" action="caLanguage">
            <s:param name="request_locale">es</s:param>
        </s:url>
        <s:a href="%{url}">Espanol</s:a>
    </li>
</ul>
    <script>
      $(document).ready(function () {
            $('#navLanguage').addClass('active');
        });
    </script>
</body>
</html>
