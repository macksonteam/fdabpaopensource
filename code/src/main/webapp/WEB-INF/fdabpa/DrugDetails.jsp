<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title><s:text name="drugs"/></title>

  <%-- Bootstrap components --%>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="../css/mackson-theme.css">

  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  <script src="../js/bootstrap.min.js"></script> 

  <style>
    .label {
      color: #000;
    }
  </style>

</head>
<body>
  <jsp:include page="Menu.jsp"/>

  <div class="container-fluid">  
    <div class="row">
      <div class="panel panel-default">
        <div class="panel-heading"><s:text name="product" /></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-6 form-group">
                <b><s:label key="spl.id" /></b><br/><s:text name="splId"/>
            </div>
            <div class="col-md-6 form-group">
                <b><s:label key="manufacturer" /></b><br/> <s:text name="manufacturerName"/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 form-group">
                <b><s:label key="product" /></b><br/> <s:text name="brandName"/>
            </div>
            <div class="col-md-6 form-group">                  
                <b><s:label key="drugType" /></b><br/> <s:text name="productType"/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 form-group">
                <b><s:label key="drugClass" /></b><br/> <s:text name="productClass"/>
            </div>              
          </div>
          <div class="row">
            <div class="col-md-12 form-group">
                <b><s:label key="warn" /></b><br/> <s:text name="warnings"/>
            </div>              
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">Adverse Events</div>
          <div class="panel-body">
              <display:table class="table table-responsive" name="adverseEvents">
                <%-- TODO:  Make this a div based layout instead of table --%>
                <%-- <display:column property="splId" title="SPL ID" /> --%>
                <display:column property="receiveDate" title="Date Received" />
                <display:column property="reactionsAsString" title="Reactions" />
              </display:table>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">Enforcement Report</div>   
            <div class="panel-body">
              <p>No Enforcement Reports Found</p>
            </div>
          </div>
        </div>     
      </div>
    </div>
    <script>
      $(document).ready(function () {
          $('#navDrugs').addClass('active');
      });
    </script>
  </body>
</html>