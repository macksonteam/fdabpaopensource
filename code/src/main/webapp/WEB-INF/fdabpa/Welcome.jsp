<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
  <title>Welcome</title>  

    <%-- Bootstrap components --%>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="../css/mackson-theme.css">

  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  <script src="../js/bootstrap.min.js"></script> 
</head>

<body>		
  <jsp:include page="Menu.jsp"/>	
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">About</div>
        <div class="panel-body">
          <h1>Drug Inspector's Tool</h1> 
          <p>The Drug Inspector’s Tool is designed to help drug inspector’s do their jobs more efficiently and more effectively while saving them time and effort.</p>
          <p>It provides inspectors with the ability to search for drugs (Structured Product Labels) by type, class, manufacturer and product name. From search results, you can dive into the details of a drug to see additional information including Adverse Events for that drug as well as any Enforcement Reports.</p>
          <p>Additionally, the Enforcements page provides an interactive map view. Browse Enforcement Reports in a geographic area and select individual reports to see their details.</p>
          <p>This is a very early prototype and your feedback would be greatly appreciated. The next release will have a built-in feedback feature. In the meantime, please send us any suggestions at <a href="mailto:mark.garrison@macksonconsulting.com">Drug Inspector Tool Feedback</a>.</p>          
        </div>
      </div>
    </div>
  </div>
  <script>
      $(document).ready(function () {
        $('#navHome').addClass('active');
    });
  </script>
</body>
</html>
