<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
    <head>
        <title>Help</title>
        <style>
            #help-section{
                padding:4px;
                margin-bottom:10px;
            }
            #screenshot{
                width:50%;
                min-width:300px;
                margin-bottom:10px;
            }
        </style>
        <!-- Bootstrap components -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/mackson-theme.css">

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="../js/bootstrap.min.js"></script> 
    </head>
    
    <body>
    	<jsp:include page="Menu.jsp"/>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Help</div>
                        <div class="panel-body">
                            
                            <div id="help-section">
                                <h4>Overview</h4>
                                <p>The Drug Inspector’s Tool is designed to help drug inspector’s do their jobs more efficiently and more effectively while saving them time and effort.</p>
                                <p>It provides inspectors with the ability to search for drugs (Structured Product Labels) by type, class, manufacturer and product name. From search results, you can dive into the details of a drug to see additional information including Adverse Events for that drug as well as any Enforcement Reports.</p>
                                <p>Additionally, the Enforcements page provides an interactive map view. Browse Enforcement Reports in a geographic area and select individual reports to see their details.</p>
                                <p>This is a very early prototype and your feedback would be greatly appreciated. The next release will have a built-in feedback feature. In the meantime, please send us any suggestions at <a href="mailto:mark.garrison@macksonconsulting.com">Drug Inspector Tool Feedback</a>.</p>
                            </div>
                            
                            <div id="help-section">
                                <h4>Target Audience / User Base</h4>
                                <p>This web application was designed for FDA personnel with the intention of aiding employees with drug related duties.</p>
                            </div>
                            
                            <div id="help-section">
                                <h4>Drug Search</h4>
                                <p>This view allows users to search for specific drug labels.  Search criteria includes:</p>
                                <ul>
                                    <li><b>Drug Type</b> Users can select from a list of available drug types.</li>
                                    <li><b>Drug Class</b> Users can select from a list of available drug classes.</li>
                                    <li><b>Manufacturer Name</b> Users type in the desired manufacturer.</li>
                                    <li><b>Product Name</b> Users can type in the desired product name.</li>
                                </ul>
                                <p>
                                    Once the user entered the desired search criteria, tapping the "Search" button will activate the search process
                                    and the results will be displayed in the area on the right side of the screen.  Clicking a result item will 
                                    navigate the user to the Drug Details view.
                                </p>
                                <div style="width:100%;">
                                	<a href="caDrugs.action"><img id="screenshot" src="../img/FDADrugFinder.png"/></a>
                                </div>
                            </div>
                            
                            <div id="help-section">
                                <h4>Drug Details</h4>
                                <p>
                                    The Drug Details view presents detailed information about the selected drug.  Here the user can also view
                                    Adverse Events and Enforcement reports related to the selected drug.  This view provides the abilities to: 
                                </p>
                                <ul>
                                    <li><b>Print</b> A detailed report will be sent to the user printer of choice.</li>
                                    <li><b>Email</b> A detailed report will be emailed to the specified email address.</li>
                                    <li><b>Share</b> A detailed report will be posted to integrated social networks.</li>
                                </ul>
                                <p>
                                    Once the user is finished viewing the details, they may return to the <a href="caDrugs.action">Drug Search</a> view to perform
                                    additional drug label searches 
                                </p>
                                <div style="width:100%;">
                                	<a href="caDrugs.action"><img id="screenshot" src="../img/FDADrugFinder2.png"/></a>
                                </div>
                            </div>
                            
                            <div id="help-section">
                            	<h4>Enforcement</h4>
                            	<p>
                            		The Enforcement view allows users to find drug manufacturers who have had drug recalls in their operational area.  
                            		Search results will be geospatially plotted on a map to show users their general location and potentially identify 
                            		drug recall trends within their area. 
                            	</p>
                            	<p>
                            		Users can search for manufacturers using the following criteria:
                            	</p>
                            	<ul>
                                    <li><b>State</b> Users can select one of the states within the United States.</li>
                                    <li><b>From Date</b> Users can select the "From" date of the drug recall "Added" date range</li>
                                    <li><b>To Date</b> Users can select the "To" date of the drug recall "Added" date range</li>
                                    <li><b>Result Limit</b> Users can select the result limit.  Faster queries can be achieved by limiting this value.</li>
                                </ul>
                                <p>
                            		Once search results are obtained, they will be displayed on the map using Map pin icons. 
                            		Clicking these icons will display detailed information about the area clicked.  From here
                            		users can continue to change the search criteria and update the results, or choose to navigate to 
                            		another portion of the application.
                            	</p>
                            	<div style="width:100%;">
                                	<a href="Enforcement"><img id="screenshot" src="../img/FDADrugFinder3.png"/></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <p>Mackson Consulting, LLC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
          $(document).ready(function () {
            $('#navHelp').addClass('active');
        });
      </script>
</html>