<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<nav class="navbar navbar-default">
  <div class="container-fluid">    
    <a href="http://macksonconsulting.com/newsite/" class="visible-xs pull-left">
      <img class="mackson-navbar-icon" 
           title="Mackson Consulting LLC"
           src="../img/mackson-sm.png"/>            
    </a>         
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fda-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>      
    </div>
    
    <div class="collapse navbar-collapse" id="fda-navbar-collapse-1">
      <ul class="nav navbar-nav">                          
        <li id="navHome"><a href="<s:url action="Welcome"/>"><s:text name="home"/></a></li>
        <li id="navDrugs"><a href="<s:url action="caDrugs"/>"><s:text name="drugs"/></a></li>
        <li id="navEnforcement"><a href="<s:url action="Enforcement"/>"><s:text name="enforcement"/></a></li>
        <li id="navLanguage"><a href="<s:url action="caLanguage"/>"><s:text name="set.language"/></a></li>
        <li id="navHelp"><a href="<s:url action="Help"/>"><s:text name="help"/></a></li>                              
      </ul>   
      <ul class="nav navbar-nav navbar-right hidden-xs">
        <a href="http://macksonconsulting.com/newsite/">
          <img class="mackson-navbar-icon" 
               title="Mackson Consulting LLC"
               src="../img/mackson-sm.png"/>
        </a> 
      </ul>
    </div><!-- /.navbar-collapse -->        
  </div><!-- /.container-fluid -->
</nav>