package com.macksonconsulting.fdabpa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.macksonconsulting.fdabpa.model.DrugLabel;
import com.macksonconsulting.fdabpa.util.FdaEncoder;
import com.macksonconsulting.fdabpa.util.JsonUtil;
import com.opensymphony.xwork2.ActionSupport;

/**
 * This is the base action from which other actions will be derived. Also used
 * for certain simple actions.
 */
public class BaseAction extends ActionSupport {
    private static final long serialVersionUID = -4070885246786530747L;

    protected static final String URL_DRUG = "https://api.fda.gov/drug/label.json?api_key=YOJqCcVyWcYwNqkudywpPncnD3qWqmTbT04Ya9Ak";

    /**
     * Given a URL parse the resultant JSON to pull out the terms.
     * 
     * @param url the url
     * @return the terms
     */
    protected static Set<String> pullTermsFromUrl(String url) {
        Set<String> result = new TreeSet<String>();
        JSONObject json = BaseAction.readJsonFromUrl(url);
        JSONArray array = json.getJSONArray("results");
        for (int i = 0; i < array.length(); i++) {
            result.add(array.getJSONObject(i).getString("term"));
        }
        return result;
    }

    /**
     * Read the JSON using HTTP Get.
     * 
     * @param url the REST call
     * @return the response JSON
     */
    protected static JSONObject readJsonFromUrl(String url) {
        LOG.info("FDA Get: " + url);
        JSONObject json = null;
        try {
            InputStream is = new URL(url).openStream();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                String jsonText = readAll(rd);
                json = new JSONObject(jsonText);
                return json;
            } finally {
                is.close();
            }
        } catch (Exception e) {
            LOG.error("Error in readJsonFromUrl()", e);
        }
        return json;
    }

    /**
     * Get a list of DrugLabel based on criteria.
     * 
     * @param criteria the selection criteria
     * @param limit the limit
     * @return a list of DrugLabel
     * @throws Exception exception
     */
    protected List<DrugLabel> queryDrug(FdaEncoder criteria, int limit) throws Exception {
        List<DrugLabel> result = new ArrayList<DrugLabel>();
        String url = URL_DRUG + criteria.toString() + "&limit=" + String.valueOf(limit);
        JSONObject json = readJsonFromUrl(url);
        if (json != null) {
            JSONArray array = json.getJSONArray("results");
            for (int i = 0; i < array.length(); i++) {
                try {
                    DrugLabel dl = new DrugLabel();
                    JSONObject obj = array.getJSONObject(i);
                    JSONObject openFda = (JSONObject) obj.get("openfda");
                    dl.setSplId(JsonUtil.getSt(openFda, "spl_id"));
                    dl.setProductType(JsonUtil.getSt(openFda, "product_type"));
                    dl.setProductClass(JsonUtil.getSt(openFda, "pharm_class_cs"));
                    dl.setBrandName(JsonUtil.getSt(openFda, "brand_name"));
                    dl.setWarnings(JsonUtil.getSt(obj, "warnings"));
                    dl.setManufacturerName(JsonUtil.getSt(openFda, "manufacturer_name"));
                    result.add(dl);
                } catch (JSONException e) {
                    LOG.trace("Exception parsing json.", e);
                }
            }
        }
        return result;
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
