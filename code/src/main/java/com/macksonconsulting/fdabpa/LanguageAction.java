package com.macksonconsulting.fdabpa;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Sample action class.
 */
public class LanguageAction extends ActionSupport {
    private static final long serialVersionUID = 1294508928444939402L;

    @Override
    public String execute() throws Exception {
        setMessage(getText(MESSAGE));
        return SUCCESS;
    }

    /**
     * Provide default value for Message property.
     */
    public static final String MESSAGE = "HelloWorld.message";

    /**
     * Field for Message property.
     */
    private String message;

    /**
     * Return Message property.
     *
     * @return message property
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set Message property.
     *
     * @param message text to display on HelloWorld page.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
