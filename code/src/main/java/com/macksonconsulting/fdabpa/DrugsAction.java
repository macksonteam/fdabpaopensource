package com.macksonconsulting.fdabpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.macksonconsulting.fdabpa.model.DrugLabel;
import com.macksonconsulting.fdabpa.util.FdaEncoder;

/**
 * Action class used for the Drugs screen.
 */
public class DrugsAction extends BaseAction {
    private static final long serialVersionUID = -9134218252599978977L;
    static final String NO_SELECTION = "empty";
    static final String URL_TYPES = URL_DRUG + "&count=product_type";
    private static final String URL_CLASSES = URL_DRUG + "&count=pharm_class_cs";

    private String drugType;
    private String drugClass;
    private String manufacturer;
    private String product;
    private List<DrugLabel> drugs = new ArrayList<DrugLabel>();

    @Override
    public String execute() throws Exception {
        return super.execute();
    }

    /**
     * Query FDA based on the criteria.
     * 
     * @return action result
     * @throws Exception an exception
     */
    public String query() throws Exception {
        setDrugs(queryDrug(generateCriteria(), 10));
        return execute();
    }

    private FdaEncoder generateCriteria() {
        FdaEncoder fda = new FdaEncoder();
        if (!NO_SELECTION.equals(drugType)) {
            fda.add("product_type", drugType);
        }
        if (!NO_SELECTION.equals(drugClass)) {
            fda.add("pharm_class_cs", drugClass);
        }
        fda.add("manufacturer_name", manufacturer);
        fda.add("brand_name", product);
        return fda;
    }

    /**
     * @return the drugTypes
     */
    public Set<String> getDrugTypes() {
        return pullTermsFromUrl(DrugsAction.URL_TYPES);
    }

    /**
     * @return the drugClasses
     */
    public Set<String> getDrugClasses() {
        return pullTermsFromUrl(DrugsAction.URL_CLASSES);
    }

    /**
     * @return the drugType
     */
    public String getDrugType() {
        return drugType;
    }

    /**
     * @param drugType the drugType to set
     */
    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    /**
     * @return the drugClass
     */
    public String getDrugClass() {
        return drugClass;
    }

    /**
     * @param drugClass the drugClass to set
     */
    public void setDrugClass(String drugClass) {
        this.drugClass = drugClass;
    }

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * @param manufacturer the manufacturer to set
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the drugs
     */
    public List<DrugLabel> getDrugs() {
        return drugs;
    }

    /**
     * @param drugs the drugs to set
     */
    public void setDrugs(List<DrugLabel> drugs) {
        this.drugs = drugs;
    }
}
