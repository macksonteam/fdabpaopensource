package com.macksonconsulting.fdabpa.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

/**
 * Class used for utilities to help with retrieving values from JSON objects.
 * TODO: look into using Gson
 */
public class JsonUtil {
    protected static Logger LOG = LoggerFactory.getLogger(JsonUtil.class);

    /**
     * Pull a date from a json object string of format yyyyMMdd.
     * 
     * @param jobj the json object
     * @param key the key
     * @return the Date, null if not found or incorrect format
     */
    public static Date getDt(JSONObject jobj, String key) {
        Date result = null;
        String dstr = null;
        try {
            Object obj = jobj.get(key);
            if (obj instanceof String) {
                dstr = (String) obj;
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                result = format.parse(dstr);
            }
        } catch (JSONException e) {
            LOG.trace("Unable to get " + key, e);
        } catch (ParseException e) {
            LOG.trace("Unable to parse date " + dstr, e);
        }
        return result;
    }

    /**
     * Pull a string from a json object.
     * 
     * @param jobj the json object
     * @param key the key
     * @return the String, null if not found
     */
    public static String getSt(JSONObject jobj, String key) {
        String result = null;
        try {
            Object obj = jobj.get(key);
            if (obj instanceof JSONArray) {
                result = ((JSONArray) obj).getString(0);
            } else if (obj instanceof String) {
                result = (String) obj;
            }
        } catch (JSONException e) {
            LOG.trace("Unable to get " + key, e);
        }
        return result;
    }

    /**
     * Pull a string from a json object.
     * 
     * @param jobj the json object
     * @param key the key
     * @return the integer, 0 if not found
     */
    public static int getInt(JSONObject jobj, String key) {
        int result = 0;
        String intString = "0";
        try {
            Object obj = jobj.get(key);
            if (obj instanceof JSONArray) {
                intString = ((JSONArray) obj).getString(0);
            } else if (obj instanceof String) {
                intString = (String) obj;
            }
            result = Integer.parseInt(intString);
        } catch (JSONException e) {
            LOG.trace("Unable to get " + key, e);
        } catch (NumberFormatException e) {
            LOG.trace("Unable to parse integer " + intString, e);
        }
        return result;
    }
}
