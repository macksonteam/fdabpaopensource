package com.macksonconsulting.fdabpa.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

/**
 * Used to encode search portion of query string.
 */
public class FdaEncoder {

    protected static Logger LOG = LoggerFactory.getLogger(FdaEncoder.class);

    private Map<String, String> criteria = new HashMap<String, String>();

    public void add(String key, String value) {
        if (StringUtils.isNotBlank(value)) {
            criteria.put(key, value.trim());
        }
    }

    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();
        if (!criteria.isEmpty()) {
            result.append("&search=");
            boolean first = true;
            for (String key : criteria.keySet()) {
                try {
                    String value = URLEncoder.encode(criteria.get(key), "UTF-8");
                    if (first) {
                        first = false;
                    } else {
                        result.append("+AND+");
                    }
                    result.append(key);
                    result.append(':');
                    result.append(value);
                } catch (UnsupportedEncodingException e) {
                    LOG.error("Encoding error", e);
                }
            }
        }
        return result.toString();
    }
}
