package com.macksonconsulting.fdabpa.model;

public class Reaction {

	// API:patient.reaction.reactionmeddrapt
	private String description;
	//API:patient.reaction.reactionoutcome
	private int reactionOutcome;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getReactionOutcome() {
		return reactionOutcome;
	}
	public void setReactionOutcome(int reactionOutcome) {
		this.reactionOutcome = reactionOutcome;
	}
}
