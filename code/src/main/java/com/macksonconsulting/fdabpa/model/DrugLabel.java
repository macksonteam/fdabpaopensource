package com.macksonconsulting.fdabpa.model;

public class DrugLabel {
	
	private String splId;
	private String productType;
	private String productClass;
	private String brandName;
	private String warnings;
	private String manufacturerName;
	
	public String getSplId() {
		return splId;
	}
	public void setSplId(String splId) {
		this.splId = splId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductClass() {
		return productClass;
	}
	public void setProductClass(String productClass) {
		this.productClass = productClass;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getWarnings() {
		return warnings;
	}
	public void setWarnings(String warnings) {
		this.warnings = warnings;
	}
	public String getManufacturerName() {
		return manufacturerName;
	}
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}
	
}
