package com.macksonconsulting.fdabpa.model;

public class ProductType {
	
	// Instance Fields
	private String productTypeId;
	private String displayName;
	
	// Properties
	public String getProductTypeId() {
		return productTypeId;
	}
	public void setProductTypeId(String productTypeId) {
		this.productTypeId = productTypeId;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
