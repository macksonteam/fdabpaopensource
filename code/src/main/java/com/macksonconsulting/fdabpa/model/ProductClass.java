package com.macksonconsulting.fdabpa.model;

public class ProductClass {
	
	// Fields
	private String productClassId;
	private String displayName;

	//Properties
	public String getProductClassId() {
		return productClassId;
	}
	public void setProductClassId(String productClassId) {
		this.productClassId = productClassId;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
