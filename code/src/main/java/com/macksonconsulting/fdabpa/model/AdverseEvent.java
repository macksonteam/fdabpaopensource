package com.macksonconsulting.fdabpa.model;

import java.util.Collection;
import java.util.Date;

public class AdverseEvent {

    private String splId;
    private Date receiveDate;
    private Collection<Reaction> reactions;

    public String getSplId() {
        return splId;
    }

    public void setSplId(String splId) {
        this.splId = splId;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Collection<Reaction> getReactions() {
        return reactions;
    }

    public void setReactions(Collection<Reaction> reactions) {
        this.reactions = reactions;
    }

    public String getReactionsAsString() {
        StringBuffer sb = new StringBuffer();
        for (Reaction r : getReactions()) {
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append(r.getDescription());
        }
        return sb.toString();
    }
}
