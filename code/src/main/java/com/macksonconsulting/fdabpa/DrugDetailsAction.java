package com.macksonconsulting.fdabpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.ParameterAware;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.macksonconsulting.fdabpa.model.AdverseEvent;
import com.macksonconsulting.fdabpa.model.DrugLabel;
import com.macksonconsulting.fdabpa.model.Reaction;
import com.macksonconsulting.fdabpa.util.FdaEncoder;
import com.macksonconsulting.fdabpa.util.JsonUtil;

public class DrugDetailsAction extends BaseAction implements ParameterAware {

    private static final long serialVersionUID = -7143972318600369140L;

    private Map<String, String[]> parameters;

    private String splId;
    private String productType;
    private String productClass;
    private String brandName;
    private String warnings;
    private String manufacturerName;
    private List<AdverseEvent> adverseEvents = new ArrayList<AdverseEvent>();

    private static final String URL_AES = "https://api.fda.gov/drug/event.json"
            + "?api_key=YOJqCcVyWcYwNqkudywpPncnD3qWqmTbT04Ya9Ak&search=spl_id:";

    @Override
    public String execute() throws Exception {
        String[] param = parameters.get("splId");
        if (param != null) {
            FdaEncoder criteria = new FdaEncoder();
            criteria.add("spl_id", param[0]);
            List<DrugLabel> drugs = queryDrug(criteria, 1);
            if (!drugs.isEmpty()) {
                DrugLabel drug = drugs.get(0);
                setSplId(drug.getSplId());
                setProductType(drug.getProductType());
                setProductClass(drug.getProductClass());
                setBrandName(drug.getBrandName());
                setWarnings(drug.getWarnings());
                setManufacturerName(drug.getManufacturerName());
                setProductType(drug.getProductType());
            }
            queryAdverseEvents(param[0]);
        }
        return super.execute();
    }

    void queryAdverseEvents(String splId) {
        adverseEvents.clear();
        String url = URL_AES + splId + "&limit=10";
        JSONObject json = readJsonFromUrl(url);
        if (json != null) {
            JSONArray array = json.getJSONArray("results");
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject obj = array.getJSONObject(i);
                    JSONObject patient = (JSONObject) obj.get("patient");
                    JSONArray reactions = (JSONArray) patient.get("reaction");
                    JSONArray drugs = (JSONArray) patient.get("drug");
                    JSONObject drug = drugs.getJSONObject(0);
                    JSONObject openFda = (JSONObject) drug.get("openfda");

                    AdverseEvent ae = new AdverseEvent();
                    ae.setSplId(JsonUtil.getSt(openFda, "spl_id"));
                    ae.setReactions(new ArrayList<Reaction>());
                    for (int j = 0; j < reactions.length(); j++) {
                        Reaction r = new Reaction();
                        r.setDescription((JsonUtil.getSt(reactions.getJSONObject(j), "reactionmeddrapt")));
                        r.setReactionOutcome(JsonUtil.getInt(reactions.getJSONObject(j), "reactionoutcome"));
                        ae.getReactions().add(r);
                    }
                    ae.setReceiveDate(JsonUtil.getDt(obj, "receivedate"));
                    adverseEvents.add(ae);
                } catch (JSONException e) {
                    LOG.trace("Exception parsing json.", e);
                }
            }
        }
    }

    /**
     * @return the adverseEvents
     */
    public List<AdverseEvent> getAdverseEvents() {
        return adverseEvents;
    }

    /**
     * @param adverseEvents the adverseEvents to set
     */
    public void setAdverseEvents(List<AdverseEvent> adverseEvents) {
        this.adverseEvents = adverseEvents;
    }

    @Override
    public void setParameters(Map<String, String[]> parameters) {
        this.parameters = parameters;
    }

    /**
     * @return the splId
     */
    public String getSplId() {
        return splId;
    }

    /**
     * @param splId the splId to set
     */
    public void setSplId(String splId) {
        this.splId = splId;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /**
     * @return the productClass
     */
    public String getProductClass() {
        return productClass;
    }

    /**
     * @param productClass the productClass to set
     */
    public void setProductClass(String productClass) {
        this.productClass = productClass;
    }

    /**
     * @return the brandName
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * @param brandName the brandName to set
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    /**
     * @return the manufacturerName
     */
    public String getManufacturerName() {
        return manufacturerName;
    }

    /**
     * @param manufacturerName the manufacturerName to set
     */
    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    /**
     * @return the parameters
     */
    public Map<String, String[]> getParameters() {
        return parameters;
    }

    /**
     * @return the warnings
     */
    public String getWarnings() {
        return warnings;
    }

    /**
     * @param warnings the warnings to set
     */
    public void setWarnings(String warnings) {
        this.warnings = warnings;
    }
}
