package com.macksonconsulting.fdabpa;

public class LanguageActionTest extends AbstractActionTest {

    @Override
    String getAction() {
        return "caLanguage";
    }

    @Override
    Class<?> getActionClass() {
        return LanguageAction.class;
    }

    public void testHelloWorld() throws Exception {
        LanguageAction hello_world = new LanguageAction();
        String result = hello_world.execute();
        assertSuccess(result);
        assertTrue("Expected the default message!",
                hello_world.getText(LanguageAction.MESSAGE).equals(hello_world.getMessage()));
    }
}
