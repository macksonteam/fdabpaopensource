package com.macksonconsulting.fdabpa;

import java.util.HashMap;

public class DrugDetailsActionTest extends AbstractActionTest {

    private DrugDetailsAction action;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        action = new DrugDetailsAction();
        action.setParameters(new HashMap<String, String[]>());
        action.execute();
    }

    @Override
    String getAction() {
        return "caDrugDetails";
    }

    @Override
    Class<?> getActionClass() {
        return DrugDetailsAction.class;
    }
}
