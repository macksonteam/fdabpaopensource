package com.macksonconsulting.fdabpa;

import java.util.Map;

import org.apache.struts2.StrutsTestCase;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.config.RuntimeConfiguration;
import com.opensymphony.xwork2.config.entities.ActionConfig;
import com.opensymphony.xwork2.config.entities.ResultConfig;
import com.opensymphony.xwork2.config.providers.XmlConfigurationProvider;

public abstract class AbstractActionTest extends StrutsTestCase {

    private static final String NAMESPACE = "/mackson";

    abstract String getAction();

    abstract Class<?> getActionClass();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        XmlConfigurationProvider c = new XmlConfigurationProvider("struts.xml");
        configurationManager.addContainerProvider(c);
        configurationManager.reload();
    }

    public void testConfig() throws Exception {
        assertNotNull(configurationManager);
    }

    public void testActionMapping() throws Exception {
        assertClass(getAction(), getActionClass().getName());
    }

    protected ActionConfig assertClass(String action_name, String class_name) {
        RuntimeConfiguration configuration = configurationManager.getConfiguration().getRuntimeConfiguration();
        ActionConfig config = configuration.getActionConfig(NAMESPACE, action_name);
        assertNotNull("Mssing action", config);
        assertTrue("Wrong class name: [" + config.getClassName() + "]", class_name.equals(config.getClassName()));
        return config;
    }

    protected void assertSuccess(String result) throws Exception {
        assertTrue("Expected a success result!", ActionSupport.SUCCESS.equals(result));
    }

    protected void assertResult(ActionConfig config, String result_name, String result_value) {
        Map results = config.getResults();
        ResultConfig result = (ResultConfig) results.get(result_name);
        Map params = result.getParams();
        String value = (String) params.get("actionName");
        if (value == null)
            value = (String) params.get("location");
        assertTrue("Wrong result value: [" + value + "]", result_value.equals(value));
    }
}
