package com.macksonconsulting.fdabpa;

import static org.junit.Assert.assertNotNull;

import org.json.JSONObject;
import org.junit.Test;

public class BaseActionTest {

    @Test
    public void readJsonFromUrl() {
        JSONObject json = BaseAction.readJsonFromUrl(DrugsAction.URL_TYPES);
        assertNotNull(json);
    }
}
