package com.macksonconsulting.fdabpa;

import java.util.Set;

import com.opensymphony.xwork2.ActionSupport;

public class DrugsActionTest extends AbstractActionTest {

    private DrugsAction action;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        action = new DrugsAction();
        action.execute();
        action.setDrugType(DrugsAction.NO_SELECTION);
        action.setDrugClass(DrugsAction.NO_SELECTION);
    }

    @Override
    String getAction() {
        return "caDrugs";
    }

    @Override
    Class<?> getActionClass() {
        return DrugsAction.class;
    }

    public void testQuery() throws Exception {
        assertEquals(ActionSupport.SUCCESS, action.query());
    }

    public void testGetDrugTypes() {
        Set<String> cs = action.getDrugTypes();
        assertTrue(cs.contains("drug"));
    }

    public void testGetDrugClasses() {
        Set<String> cs = action.getDrugClasses();
        assertTrue(cs.contains("chemical"));
    }
}
