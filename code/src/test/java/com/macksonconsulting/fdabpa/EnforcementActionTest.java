package com.macksonconsulting.fdabpa;

public class EnforcementActionTest extends AbstractActionTest {

    @Override
    String getAction() {
        return "Enforcement";
    }

    @Override
    Class<?> getActionClass() {
        return BaseAction.class;
    }
}
