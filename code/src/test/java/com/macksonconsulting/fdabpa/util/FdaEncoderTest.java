package com.macksonconsulting.fdabpa.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FdaEncoderTest {

    @Test
    public void testToString() {
        FdaEncoder fe = new FdaEncoder();
        fe.add("key", "one two  three   ");
        assertEquals("&search=key:one+two++three", fe.toString());
    }
}
