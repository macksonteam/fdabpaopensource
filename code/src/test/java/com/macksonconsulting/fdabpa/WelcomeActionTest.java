/**
 * 
 */
package com.macksonconsulting.fdabpa;

public class WelcomeActionTest extends AbstractActionTest {

    @Override
    String getAction() {
        return "Welcome";
    }

    @Override
    Class<?> getActionClass() {
        return BaseAction.class;
    }
}
