Instruction to build and test Mackson FDA BPA project:

1. Install Java 7 (jdk 1.7)

2. Install apache-maven-3.3.3

3. To build and run unit tests from command prompt run "mvn clean package".

4. To integration test:
    - from command prompt run "mvn jetty:run-war" 
    - point browser at http://localhost:8080/mackson
